<?php namespace App;

use Log;
use File;
use Illuminate\Database\Eloquent\Model;
use Symfony\Component\HttpKernel\Exception;

class MagazineItem extends Model {

	protected $table = "items";

	protected $fillable = ['name', 'image_url', 'image_path'];

	public function remove_file () {

		try {

			\File::Delete($this->image_path);
			
		} catch (\Exception $e) {

			Log::error($e->getMessage());
		}
		
	}
}