<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Magazine extends Model {

	protected $table = "magazines";

	protected $fillable = ['name', 'description'];

	public function items () {

		return $this->hasMany('App\MagazineItem');
	}

	public function first_item () {

		return MagazineItem::where('magazine_id', $this->id)->orderBy('index', 'ASC')->first();
	}
}