<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class ActionDone extends Event
{
    use SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        /*
        
        $data = ['body'         => $event->contactMailHelper->emailBody,
                 'emailAddress' => $event->contactMailHelper->emailAddress,
                 'emailSubject' => $event->contactMailHelper->emailSubject,
                 'emailView'    => $event->contactMailHelper->emailView,
        ];

        Mail::queue($data['emailView'],$data , function($message) use($data)
        {
            $message->to($data['emailAddress'])->subject($data['emailSubject']);
        });

        */
        return [];
    }
}
