<?php

function nav_option_active ($module) {

	$path = Request::segment(2);

	if ($module == $path) {
		
		return 'active';
	}
}

function magazine_base_image_to_image ($data) {

	$folder 	= '/files/magazines/';

	/* Image type */
	$data_type 	= explode(";", $data);
	$data_type 	= $data_type[0];
	$array_type = explode('/', $data_type);
	$type 		= $array_type[1];

	/* Convert */
	$image 		= $data;
	$image 		= str_replace('data:image/' . $type . ';base64,', '', $image);
	$image 		= str_replace(' ', '+', $image);
	$base64 	= base64_decode($image);

	/* Name */
	$name 		= "magazine-" . uniqid() . date('ymd') . '.' . $type;
	$file 		= $folder . $name;
	$image_path = base_path('public/' . $file);
	$success 	= file_put_contents($image_path, $base64);

	if (!$success) {
		throw new \Exception("Error Processing Image");
	}
	
	return $file;
}

function redirect_back () {

	if (isset($_SERVER['HTTP_REFERER']) === true) {

		$url = $_SERVER['HTTP_REFERER'];
		
	} else {

		if (Auth::check()) {
			
			$user = Auth::user();

			if (!$user->role_user()) {
				
				return route('website.index');	
			}

			$url = route('panel.magazines.index');

		} else {

			$url = route('website.index');
		}
	}

	return $url;
}