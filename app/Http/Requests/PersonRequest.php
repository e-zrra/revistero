<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PersonRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'company' => 'required',
            'name' => 'required',
            'email' => 'required|email|unique:people'
        ];
    }

    public function messages()
    {
        return [
            'company.required' => 'The company name is required',
            'name.required'  => 'The name field is required',
            'email.required' => 'The email field is required',
            'email.email' => 'Email field is invalid format',
            'email.unique' => 'The email is already registered'
        ];
    }
}