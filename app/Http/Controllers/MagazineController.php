<?php namespace App\Http\Controllers;

use Log;
use App\Magazine;
use App\MagazineItem;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MagazineController extends Controller
{
    public function index ()
	{
		$magazines = Magazine::all();

		return view('panel.magazines.index', array('magazines' => $magazines));
	}

	public function create ()
	{
		$magazine = Magazine::create(array('name' => '', 'description' => ''));

		return redirect()->route('panel.magazines.edit', ['id' => $magazine->id]);
	}

	public function edit ($id) {

		$magazine = Magazine::find($id);

		$items = MagazineItem::where('magazine_id', $magazine->id)->orderBy('index', 'ASC')->get();

		return view('panel.magazines.edit', array('magazine' => $magazine, 'items' => $items));
	}

	public function update ($id, Request $request) {

		$magazine = Magazine::find($id);

		$response = ['success' => null, 'data' => null, 'msg' => null];

		try {

			$magazine->name = $request->input('name');

			$magazine->description = $request->input('description');

			$magazine->save();

			$response["msg"] = 'Se almaceno correctamente';
			
		} catch (Exception $e) {

			Log::error($e);

			$response["error"]= $e->getMessage();
			
		} finally {

			return response()->json($response);
		}
	}

	public function item_store (Request $request) {

		$response = ['success' => null, 'data' => null];

		try {

			$magazine_id = $request->input('magazine_id');
            
			if ( null !== $request->input('value') && null !== $request->input('magazine_id')) {
				
				$name = magazine_base_image_to_image($request->input('value'));

				$last_item = MagazineItem::where('magazine_id', $magazine_id)
								->orderBy('index', 'DESC')
								->first();

				$item = new MagazineItem;

				$item->name = $name;

				$item->image_url = url($name);

				$item->image_path = base_path('public/' . $name);

				$item->magazine_id = $magazine_id;

				$item->index = ($last_item) ? $last_item->index + 1 : 0;

				$item->save();

				$response['data'] = $item;

			} else {

				throw new \Exception("Sin archivo o Identificador de revista");
			}

			$response["success"] = 'Se almaceno correctamente';
            

		} catch (\Exception $e) {
			
			Log::error($e);

			$response["error"]= $e->getMessage();
            
		} finally {

			return response()->json($response);
		}
		
	}

	public function update_index ($id, Request $request) {

		$response = ['success' => null, 'data' => null];

		try {

			$prev_item = MagazineItem::where('magazine_id', $request->input('magazine_id'))
						->where('index', $request->input('index'))
						->first();

			$prev_item = MagazineItem::find($prev_item->id);

			$prev_item->index = $request->input('prev_index');

			$prev_item->save();

			$item = MagazineItem::find($id);

			$item->index =  $request->input('index');

			$item->save();

			$response["success"] = 'Orden de revista actualizada';
			
		} catch (\Exception $e) {
			
			Log::error($e);

			$response["error"]= $e->getMessage();

		} finally {

			return response()->json($response);
		}

	}

	public function item_destroy ($id) {

		$response = ['success' => null, 'data' => null];

		try {

			$item = MagazineItem::find($id);

			$item->delete();

			$response["success"] = 'Elemento eliminado';
			
		} catch (\Exception $e) {
			
			Log::error($e);

			$response["error"]= $e->getMessage();

		} finally {

			return response()->json($response);
		}

	}
}
