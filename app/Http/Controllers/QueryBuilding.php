<?php namespace App\Http\Controllers;

use DB;
use App\Http\Controllers\Controller;

class QueryBuildingController extends Controller
{
	public function index()
    {
    	/**
    	 * SIMPLE
    	 */
        $users = DB::table('users')->get();

        /**
         * WHERE
         */
        $user = DB::table('users')->where('name', 'John')->first();

        /**
         * ARRAY
         */
        $titles = DB::table('roles')->lists('title');

        /**
         * AGGREGATES
         */

        $users = DB::table('users')->count();

		$price = DB::table('orders')->max('price');

		// CONBINE

		$price = DB::table('orders')
                ->where('finalized', 1)
                ->avg('price');

        /**
         * SELECT
         */

        $users = DB::table('users')->select('name', 'email as user_email')->get();

        /**
         * EXPRESSIONS
         */
        $users = DB::table('users')
                     ->select(DB::raw('count(*) as user_count, status'))
                     ->where('status', '<>', 1)
                     ->groupBy('status')
                     ->get();

        /**
         * JOINS
         */

        $users = DB::table('users')
            ->join('contacts', 'users.id', '=', 'contacts.user_id')
            ->join('orders', 'users.id', '=', 'orders.user_id')
            ->select('users.*', 'contacts.phone', 'orders.price')
            ->get();


        // return view('user.index', ['users' => $users]);

        /**
         * INSERTS
         */

        DB::table('users')->insert([
		    ['email' => 'taylor@example.com', 'votes' => 0],
		    ['email' => 'dayle@example.com', 'votes' => 0]
		]);

		/**
		 * UPDATE
		 */

		DB::table('users')
            ->where('id', 1)
            ->update(['votes' => 1]);

        /**
         * DELETE AND TRUNCATE
         */

        DB::table('users')->where('votes', '<', 100)->delete();

    }
}