<?php namespace App\Http\Controllers;

use App\Magazine;
use App\MagazineItem;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class WebsiteController extends Controller
{
    public function index ()
	{
		$magazines = Magazine::take(10)->orderBy('id', 'DESC')->get();

		return view('website.index', array('magazines' => $magazines));
	}

	public function magazine_show ($id) {

		$magazine = Magazine::find($id);

		$items = MagazineItem::where('magazine_id', $magazine->id)
					->orderBy('index', 'ASC')
					->get();

		return view('website.magazine.show', array('magazine' => $magazine, 'items' => $items));
	}

	public function magazine_details ($id) {

		$magazine = Magazine::find($id);

		$item = MagazineItem::where('magazine_id', $magazine->id)
					->orderBy('index', 'ASC')
					->first();

		return view('website.magazine.details', 
			array('magazine' => $magazine, 'item' => $item));
	}
}
