<?php namespace App\Http\Controllers;

use App\Magazine;
use App\MagazineItem;
use Illuminate\Http\Request;
use App\Http\Requests\PostRequest;
use App\Http\Controllers\Controller;

class PanelController extends Controller
{
    public function index ()
	{
		return view('panel.index');
	}
}
