<?php namespace App\Http\Controllers;

use App\Magazine;
use App\MagazineItem;
use App\Http\Controllers\Controller;

class AnalyticController extends Controller
{
    public function index ()
	{
		$magazines = Magazine::all();

		$magazines_top = Magazine::take(5)->orderBy('id', 'DESC')->get();

		return view('panel.analytic.index', 
					array('magazines' => $magazines, 'magazines_top' => $magazines_top));
	}

	public function show ($id) {

		return view('panel.analytic.show', array());
	}
}
