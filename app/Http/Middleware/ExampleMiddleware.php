<?php

namespace App\Http\Middleware;

use Closure;

class ExampleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (date('w') === 5) {

            echo "Friday";

        } else {

            echo ":(";
        }

        return $next($request);

        /**
         * AFTER
         */

        /*
            $response = $next($request);

            // Perform action

            return $response;
        */
    }
}
