<?php

Route::get('/', array( 'as' => 'website.index', 'uses' => 'WebsiteController@index'));

Route::get('revista/detalles/{id}', array( 'as' => 'website.magazine.details', 'uses' => 'WebsiteController@magazine_details'));

Route::get('revista/visor/{id}', array( 'as' => 'website.magazine.show', 'uses' => 'WebsiteController@magazine_show'));

Route::get('panel', array( 'as' => 'panel.index', 'uses' => 'PanelController@index'));

Route::get('panel/magazines', array('as' => 'panel.magazines.index', 'uses' => 'MagazineController@index'));

Route::get('panel/magazines/create', array('as' => 'panel.magazines.create', 'uses' => 'MagazineController@create'));

Route::get('panel/magazines/{id}/edit', array('as' => 'panel.magazines.edit', 'uses' => 'MagazineController@edit'));

Route::put('panel/magazines/{id}', array('as' => 'panel.magazines.update', 'uses' => 'MagazineController@update'));

Route::post('panel/magazines/item/store', array('as' => 'panel.magazines.item.store', 'uses' => 'MagazineController@item_store'));

Route::put('panel/magazines/item/update-index/{id}', array('as' => 'panel.magazines.item.update_index', 'uses' => 'MagazineController@update_index'));

Route::delete('panel/magazines/item/destroy/{id}', array('as' => 'panel.magazines.item.destroy', 'uses' => 'MagazineController@item_destroy'));

Route::get('panel/analytic', array('as' => 'panel.analytic.index', 'uses' => 'AnalyticController@index'));

Route::get('panel/analytic/{id}', array('as' => 'panel.analytic.show', 'uses' => 'AnalyticController@show'));

/**
 * ROUTE PARAMETERS, NAMED ROUTES
 */

/**
 * BASIC ROUTING
 */

Route::get('foo', function () {

	return 'Whats up';
});

/**
 * OPTIONAL PARAMETERS
 */

Route::get('users/{name?}', function ($name = null) {

	return $name;
});

/**
 * REGULAR EXPRESSION
 */

Route::get('users/{name}', function ($name) {


})->where('name', '[A-Za-z]+');


/**
 * ROUTE GROUPS
 */

Route::group(['middleware' => 'auth'], function () {

    Route::get('/', function ()    {
        // Uses Auth Middleware
    });

    Route::get('user/profile', function () {

        // Uses Auth Middleware
    });
});

/**
 * SUB-DOMAIN ROUTING
 */

Route::group(['domain' => '{account}.myapp.com'], function () {
    Route::get('user/{id}', function ($account, $id) {
        //
    });
});

/**
 * Route Prefixes
 */

Route::group(['prefix' => 'admin'], function () {
    Route::get('users', function ()    {
        // Matches The "/admin/users" URL
    });
});

/**
 * ACCESING THE CURRENT ROUTES
 */

$route = Route::current();





/**
 * MIDDLEWARE
 */

Route::get('/test', ['middleware' => 'friday', function () {

    return 'Test example middleware';
}]);



