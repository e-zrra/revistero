@extends('layouts.panel')
@section('content')
	<div class="row">
		<div class="col-md-6">
			<h3 class="mt0">Mis revistas</h3>
			<small>Visualiza tus registros</small>
		</div>
		<div class="col-md-6">
			<div align="right">
				<a href="{{ route('panel.magazines.create') }}" class="ion-plus-round btn btn-success"> Nueva revista</a>
			</div>
		</div>
	</div>
	<hr />
	<div class="row">
		@foreach($magazines as $magazine)
		<div class="col-md-2">
			<div class="form-group">
				<a href="{{ route('panel.magazines.edit', array('id' => $magazine->id)) }}">
				@if($magazine->first_item())
					<div class="content-magazine-opacity content-magazine content-magazine-large mb20" style="background-image: url({{ ($magazine->first_item()) ? $magazine->first_item()->image_url : '/' }}); background-size: cover;"></div>
				@else
					Ver detalles
				@endif
				</a>
			</div>
			<div>
				<div class="form-group">
					<a href="{{ route('panel.magazines.edit', array('id' => $magazine->id)) }}">
						{{ $magazine->name }}
					</a>
				</div>
				<div class="form-group">
					<i class="icon ion-document"></i> {{ count($magazine->items) }} página(s)
				</div>
				<div class="form-group">
					<label class="label label-success">
						Activa
					</label>
				</div>
			</div>
			<hr />
		</div>
		@endforeach
	</div>
	@if(count($magazines) == 0)
		<i>Aun sin registros</i>
	@endif
@stop
