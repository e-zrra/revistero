@extends('layouts.panel')
@section('content')

<div>
	<div class="row">
		<div class="col-md-6">
			<h3 class="m0">Revista</h3>
			<small></small>
		</div>
		<div class="col-md-6">
			<div class="mt5" align="right">
				<a class="ion-plus-round text-success" href="#">
					Agregar imagen
				</a>
				<span class="bg-gray-light mlr5">|</span> 
				<a class="icon ion-ios-book" target="_blank" href="{{ route('website.magazine.show', array('id' => $magazine->id)) }}">
					<b> Ver revista </b>
				</a> 
				<span class="bg-gray-light mlr5">|</span> 
				<a class="icon ion-arrow-graph-up-right" href="{{ route('panel.analytic.show', array('id' => $magazine->id)) }}">
					<b> Analitico </b>
				</a>
				<span class="bg-gray-light mlr5">|</span>
				<a class="ion-android-arrow-back text-warning" href="{{ redirect_back() }}">
					Regresar 
				</a>
			</div>
		</div>
	</div>
	<hr />
	<div>
		<div class="row">
			<div class="col-md-9">
				<div class="row" id="container-magazine-items">
					<div class="col-md-3">
						<div id="content-drag-drop-image" class="content-drag-drop-image" ondragover="return false">
							Arrastra imagenes aquí <br />
							<i class="plus-icon ion-plus-round color-success"></i>
						</div>
					</div>
					<div id="content-magazine-items">
						@foreach($items as $item)
							<div class="col-md-3">
								<div class="content-magazine content-magazine-large mb40" style="background-image: url({{ $item->image_url }}); background-size: cover;" data-id="{{ $item->id }}">
									<div class="magazine-content-actions">
										<a class="magazine-item-delete ion-android-delete color-danger pointer"> Eliminar </a>
									</div>
								</div>
							</div>
						@endforeach
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="panel panel-default">
					<div class="panel-body">
						{!! Form::model($magazine, ['route' => ['panel.magazines.update', $magazine->id], 'method'=>'put', 'id' => 'form-magazine-edit']) !!}
						{!! Form::token(); !!}
						{!! csrf_field() !!}
						<!-- <input type="hidden" name="_token" value="{{ csrf_token() }}"> -->

						<input type="hidden" id="magazine_id" name="magazine_id" value="{{ $magazine->id }}" />
						<div class="form-group">
							<label>Nombre de la revista</label>
							<input type="text" value="{{ $magazine->name }}" class="form-control" name="name" />
						</div>
						<div class="form-group">
							<label>Descripción</label>
							<textarea class="form-control" name="description" rows="16">{{ $magazine->description }}</textarea>
						</div>
						<div class="well">
							Tamaño de las imagenes recomendadas: 280 x 380, 480 x 620 o 680 x 820 pixeles
						</div>
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>

	var drag_drop = new DragDrop();
	
		drag_drop.init();

</script>
@stop
