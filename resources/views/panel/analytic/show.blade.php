@extends('layouts.panel')
@section('content')
<div class="alert alert-info">
	<i class="icon ion-information-circled"></i> Información ficticia, temporalmente.
</div>
<div class="row">
	<div class="col-md-6">
		<h3 class="m0">Analisis</h3>
	</div>
	<div class="col-md-6" align="right">
		<a href="{{ redirect_back() }}" class="text-warning">
			<i class="ion-android-arrow-back"></i> Regresar 
		</a>
	</div>
</div>
<hr />
<div class="row">
	<div class="col-md-3">
		<h4 class="ion-eye"> Total de vistas</h4>
		<div>
			1500
		</div>
	</div>
	<div class="col-md-3">
		<h4 class="ion-calendar"> Última fecha que fue visto</h4>
		<div>
			14 de Febrero del 2015
		</div>
	</div>
	<div class="col-md-4">
		<h4 class="ion-android-download"> Descargas</h4>
		<div>
			Número de veces que sea descargado: <b>120</b>
		</div>
	</div>
</div>
<hr />
<div class="row">
	<div class="col-md-6">
		<h4 class="mt0">Gráfica mensual</h4>
	</div>
	<div class="col-md-6">
		<span class="color-red ion-android-checkbox-blank"></span> Descargas
		<span class="bg-gray-light mlr5">|</span>
		<span class="color-blue ion-android-checkbox-blank"></span> Vistas
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<div style="width:100%;">
			<canvas id="magazine-analytic-chart" height="100"></canvas>
		</div>
	</div>
</div>


<script type="text/javascript">

	Chart.defaults.global.responsive = true;

	var ctx = document.getElementById("magazine-analytic-chart").getContext("2d");
	/*
		Color Green => Revista vista
		Color Red => Revista descargada
	*/
	var data = {
	    labels: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio"],
	    datasets: [
	        {
	            label: "Revista vista",
	            fillColor: "rgba(52, 152, 219,0.3)",
	            strokeColor: "#2980b9",
	            highlightStroke: "rgba(220,220,220,1)",
	            data: [10, 15, 20, 10, 20, 10, 10]
	        },
	        {
	            label: "Revista descargada",
	            fillColor: "rgba(231, 76, 60, 0.1)",
	            strokeColor: "#c0392b",
	            highlightStroke: "rgba(151,187,205,1)",
	            data: [50, 48, 40, 1, 40, 30, 12]
	        }
	    ]
	};

	var myBarChart = new Chart(ctx).Line(data);



</script>
@stop