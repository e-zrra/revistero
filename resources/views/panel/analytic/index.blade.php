@extends('layouts.panel')
@section('content')
	<div class="row">
		<div class="col-md-6">
			<h3 class="mt0">Analitico</h3>
			<small>Visualiza tu ...</small>
		</div>
		<div class="col-md-6">
		</div>
	</div>
	<hr />
	<div class="row">
		<div class="col-md-9">
			<table class="table table-hover">
				<thead>
					<th>
						#
					</th>
					<th>
						Nombre de la revista
					</th>
					<th>
						Páginas
					</th>
					<th>
						Fecha de la creación
					</th>
					<th>
						Ver analitico
					</th>
				</thead>
				<tbody>
					@foreach($magazines as $key => $magazine)
						<tr>
							<td> {{ $key + 1 }} </td>
							<td>
								{{ $magazine->name }}
							</td>
							<td>
								{{ count($magazine->items) }} página(s)
							</td>
							<td>
								{{ date('d/M/Y', strtotime($magazine->created_at)) }}
							</td>
							<td>
								<a href="{{ route('panel.analytic.show', array('id' => $magazine->id)) }}" class="btn btn-primary btn-primary-reverse ion-stats-bars"> Analisis </a>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
		<div class="col-md-3">
			<div class="panel panel-default">
				<div class="panel-heading">
					Lo más visto
				</div>
				<div class="panel-body">
					@foreach($magazines_top as $magazine)
					<div class="media">
					  	<div class="media-left">
					    	<a href="{{ route('panel.magazines.edit', array('id' => $magazine->id)) }}">
					      		<div class="content-magazine content-magazine-small" style="width: 60px;background-image: url({{ ($magazine->first_item()) ? $magazine->first_item()->image_url : '/' }}); background-size: cover;"></div>
					    	</a>
					  	</div>
					  	<div class="media-body">
					    	<a href="{{ route('panel.magazines.edit', array('id' => $magazine->id)) }}">
					    		<h4 class="media-heading">{{ $magazine->name }}</h4>
					    	</a>
					    	<div>
					    		{{ str_limit($magazine->description, 70, '...') }}
					    	</div>
					  	</div>
					</div>
					@endforeach
				</div>
			</div>
		</div>
	</div>
	
		

	@if(count($magazines) == 0)
		<i>Aun sin registros</i>
	@endif
@stop
