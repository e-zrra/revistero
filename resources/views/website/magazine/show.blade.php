@extends('layouts.website')
@section('content')
<div class="magazine-visor">
	<div class="container">
		<div class="section" align="center">
			<h3 class="color-white fw500">{{ str_limit($magazine->name, 50, '...') }}</h3>
		</div>

		<div class="flipbook-viewport" align="center">
			<div>
				<div class="flipbook">
					@foreach($items as $item)
						<div style="background-image:url({{ $item->image_url }}); background-size: 100% 100%;"></div>
					@endforeach
				</div>
			</div>
		</div>
	</div>
	<br />
	<div align="center" class="form-group">
		<a target="_blank" href="{{ route('website.magazine.show', array('id' => $magazine->id)) }}" class="btn icon btn-danger ion-android-download"> 
			Descargar
		</a>
		<a href="#" class="btn bg-color-facebook content-share-magazine">
			<i class="ion-social-facebook"></i>
		</a>
		<a href="#" class="btn bg-color-twitter content-share-magazine">
			<i class="ion-social-twitter"></i>
		</a>
		<a href="#" class="btn bg-color-email content-share-magazine">
			<i class="ion-email"></i>
		</a>
	</div>
</div>
<div class="container mt20">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div id="disqus_thread"></div>
		</div>
	</div>
	<script>
	/**
	* RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
	* LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables
	*/
	/*
	var disqus_config = function () {
	this.page.url = PAGE_URL; // Replace PAGE_URL with your page's canonical URL variable
	this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
	};
	*/
	(function() { // DON'T EDIT BELOW THIS LINE
	var d = document, s = d.createElement('script');

	s.src = '//revisteo.disqus.com/embed.js';

	s.setAttribute('data-timestamp', +new Date());
	(d.head || d.body).appendChild(s);
	})();
	</script>
	<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>
</div>
	
<script type="text/javascript">

	/* width:922, height:600, */
	function load_app () {
		$('.flipbook').turn({
			width:922,
			height:600,
			elevation: 50,
			gradients: true,
			autoCenter: true
		});
		$(".flipbook").click(function(){
			$(".flipbook-viewport").zoom({
		        flipbook: $(".flipbook"),
		        max:3
	        });
        });

		/* $(".flipbook").bind("zooming", function(event,  newZoomValue, currentZoomValue) {
		  alert("New zoom: "+currentZoomValue);
		}); */
	}
	yepnope({
		test : Modernizr.csstransforms,
		yep: ['../../vendor/turnjs4/lib/turn.js'],
		nope: ['../../vendor/turnjs4/lib/turn.html4.min.js'],
		both: ['../../vendor/turnjs4/lib/zoom.min.js'], //'../../vendor/turnjs4/samples/basic/css/basic.css'
		complete: load_app
	});
</script>
@stop
