@extends('layouts.website')
@section('content')
	<div class="container mt20">
		
		<div class="row">
			<div class="col-md-4">
				@if($item)
					<div class="content-magazine content-magazine-large" style="background-image: url({{ ($item->image_url) ? $item->image_url : '/' }}); background-size: cover;"></div>
				@endif
				<br />
				<div align="center">
					<a target="_blank" href="{{ route('website.magazine.show', array('id' => $magazine->id)) }}" class="btn icon btn-primary ion-ios-book"> 
						Ver completo 
					</a>
					<a target="_blank" href="{{ route('website.magazine.show', array('id' => $magazine->id)) }}" class="btn icon btn-danger ion-android-download"> 
						Descargar
					</a>
				</div>
				<br />
				<div align="center">
					<a href="#" class="btn bg-color-facebook content-share-magazine">
						<i class="ion-social-facebook"></i>
					</a>
					<a href="#" class="btn bg-color-twitter content-share-magazine">
						<i class="ion-social-twitter"></i>
					</a>
					<a href="#" class="btn bg-color-email content-share-magazine">
						<i class="ion-email"></i>
					</a>
				</div>
				
			</div>
			<div class="col-md-5">
				<h3 class="mt0"> {{ $magazine->name }} </h3>
				<p>
					{{ str_limit($magazine->description, 2000, '...') }}
				</p>
			</div>
			<div class="col-md-2 col-md-offset-1">
				<h4 class="mt0 fw500">Revistas relacionadas</h4>
				<div class="well">
					<div class="form-group">
						<img src="{{ url() }}/images/recomendada.jpg" class="img-responsive" />
					</div>
					<h5>Chin chin ... Paz y felicidad</h5>
					<hr />
					<div class="form-group">
						<img src="{{ url() }}/images/revista.jpg" class="img-responsive" />
					</div>
					<h5>Para ti ... moda</h5>
				</div>
			</div>
		</div>
		<hr />
		<div class="row">
			<div class="col-md-9">
				<div id="disqus_thread"></div>
			</div>
		</div>
		<script>
		/**
		* RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
		* LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables
		*/
		/*
		var disqus_config = function () {
		this.page.url = PAGE_URL; // Replace PAGE_URL with your page's canonical URL variable
		this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
		};
		*/
		(function() { // DON'T EDIT BELOW THIS LINE
		var d = document, s = d.createElement('script');

		s.src = '//revisteo.disqus.com/embed.js';

		s.setAttribute('data-timestamp', +new Date());
		(d.head || d.body).appendChild(s);
		})();
		</script>
		<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>
		
	</div>
@stop
