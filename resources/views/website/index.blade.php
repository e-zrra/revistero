@extends('layouts.website')
@section('content')
	<div class="container">
		<div class="mt20 alert alert-info">
			<i class="icon ion-information-circled"></i> Información ficticia, temporalmente.
		</div>
    	<div class="row">
    		<div class="col-md-9">
    			<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
				  	<!-- Indicators -->
				  	<ol class="carousel-indicators">
					    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
					    <!-- <li data-target="#carousel-example-generic" data-slide-to="1"></li>
					    <li data-target="#carousel-example-generic" data-slide-to="2"></li> -->
				  	</ol>

				  	<!-- Wrapper for slides -->
				  	<div class="carousel-inner" role="listbox">
					    <div class="item active">
					      	<img class="img-responsive" width="100%" src="{{ url() }}/images/slider/index.jpg" alt="...">
					      	<div class="carousel-caption">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					      	tempor incididunt ut labore et dolore magna aliqua.</div>
					    </div>
				  	</div>

				  	<div class="carousel-inner" role="listbox">
					    <div class="item">
					      	<img class="img-responsive" width="100%" src="{{ url() }}/images/slider/index.jpg" alt="...">
					      	<div class="carousel-caption">Anunciando</div>
					    </div>
				  	</div>

				  	<div class="carousel-inner" role="listbox">
					    <div class="item">
					      	<img class="img-responsive" width="100%" src="{{ url() }}/images/slider/index.jpg" alt="...">
					      	<div class="carousel-caption">Hola</div>
					    </div>
				  	</div>

				  	<!-- Controls -->
				  	<!-- <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
				    	<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
				    	<span class="sr-only">Previous</span>
				  	</a>
				  	<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
				    	<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
				    	<span class="sr-only">Next</span>
				  	</a> -->
				</div>
    		</div>
    	</div>
	</div>
	<hr />
	<div class="container">
		<div class="row">
			<div class="col-md-9">
				<div class="row">
					@foreach($magazines as $magazine)
					<div class="col-md-3">
						<div class="form-group">
							<a href="{{ route('website.magazine.details', array('id' => $magazine->id)) }}">
							@if($magazine->first_item())
								<div class="content-magazine-opacity content-magazine content-magazine-small" style="background-image: url({{ ($magazine->first_item()) ? $magazine->first_item()->image_url : '/' }}); background-size: cover;"></div>
							@endif
							</a>
						</div>
						<div>
							<div class="form-group">
								<a href="{{ route('website.magazine.details', array('id' => $magazine->id)) }}">
									<h4 class="title-magazine">{{ str_limit($magazine->name, 20, '..') }}</h4>
								</a>
							</div>
						</div>
						<hr />
					</div> 
					@endforeach
				</div>
			</div>
			<div class="col-md-3">
				<h2 class="mt0 fw300">Recomendado</h2>
				<div class="well">
					<img src="images/recomendada.jpg" class="img-responsive" />
				</div>
				<div>
					<h4>Chin chin ... Paz y felicidad</h4>
					Del fucsia al rosa bebé, este color tiñe prendas y accesorios y logra que tu guardarropas se vuelva más cute. 
					Delicado y femenino, se impone en los días cálidos.	
				</div>
			</div>
		</div>
		
	</div>
@stop
