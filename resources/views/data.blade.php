<!-- Print -->
Hello, {{ $name }}.

<!-- Javascript Framework -->

Hello, @{{ name }}.

<!-- If -->

{{ isset($name) ? $name : 'Default' }}

{{ $name or 'Default' }}

<!-- htmlentities -->

Hello, {!! $name !!}.