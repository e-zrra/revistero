<!DOCTYPE html>
<html>
<head>
	<title>Revisteo - Usuario panel</title>
	<link rel="stylesheet" type="text/css" href="{{ url('vendor/bootstrap/dist/css/bootstrap.min.css') }}" />
	<link rel="stylesheet" type="text/css" href="{{ url('assets/stylesheets/panel/sidebar.css') }}" />
	<link href='https://fonts.googleapis.com/css?family=Roboto:400,500,300' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" type="text/css" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
	<link rel="stylesheet" type="text/css" href="{{ url('assets/stylesheets/panel/panel.css') }}" />

	<script type="text/javascript" src="{{ url('vendor/jquery/dist/jquery.js') }}"></script>
	<script type="text/javascript" src="{{ url('vendor/jquery-ui/jquery-ui.js') }}"></script>
	<script type="text/javascript" src="{{ url('vendor/bootstrap/dist/js/bootstrap.min.js') }}"></script>
	<script type="text/javascript" src="{{ url('vendor/Chart.js/Chart.js') }}"></script>
	<script type="text/javascript" src="{{ url('vendor/jquery.form.js') }}"></script>
	<script type="text/javascript" src="{{ url('vendor/jquery.validate.min.js') }}"></script>
	<script type="text/javascript" src="{{ url('assets/javascripts/helpers.js') }}"></script>
	<script type="text/javascript" src="{{ url('assets/javascripts/panel/data.js') }}"></script>
	<script type="text/javascript" src="{{ url('assets/javascripts/panel/magazines.js') }}"></script>
</head>
<body>
	<div class="message-notification" id="message-notification"> Mensaje </div>
	<nav class="navbar navbar-default navbar-fixed-top">
      	<!--<div class="container">
        	<div class="navbar-header">
          		<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
		            <span class="sr-only">Toggle navigation</span>
		            <span class="icon-bar"></span>
		            <span class="icon-bar"></span>
		            <span class="icon-bar"></span>
          		</button>
          		<a class="navbar-brand" href="#">Revisteo</a>
        	</div>

        	<div class="collapse navbar-collapse" id="">
	          	<ul class="nav navbar-nav navbar-right">
	          	</ul>
	        </div>
      	</div>-->
    </nav>
    <input type="hidden" value="{{ url() }}" id="url" />
    <div class="container-fluid">
      	<div class="row">
	        <div class="col-sm-2 col-md-2 col-md-offset-1 sidebar">
	          	<ul class="nav nav-sidebar">
		            <li class="{{ nav_option_active('') }}">
		            	<a href="{{ route('panel.index') }}"> <i class="icon ion-home"></i> Inicio <span class="sr-only">(current)</span></a>
		            </li>
		            <li class="{{ nav_option_active('magazines') }}">
		            	<a href="{{ route('panel.magazines.index') }}"> <i class="icon ion-ios-book"></i> Mis revistas</a>
		            </li>
		            <li class="{{ nav_option_active('analytic') }}">
		            	<a href="{{ route('panel.analytic.index') }}"> <i class="icon ion-arrow-graph-up-right"></i> Analítico</a>
		            </li>
	          	</ul>
	          	<ul class="nav nav-sidebar">
		            <li><a href="#"> <i class="icon ion-person-stalker"></i> Usuarios</a></li>
		             <li><a href="#"> <i class="icon ion-ios-gear"></i> Configuración</a></li>
	          	</ul>
	          	<!--<ul class="nav nav-sidebar">
		            <li><a href="">Nav item</a></li>
		            <li><a href="">Nav item again</a></li>
		            <li><a href="">One more nav</a></li>
		            <li><a href="">Another nav item</a></li>
		            <li><a href="">More navigation</a></li>
	          	</ul>
	          	<ul class="nav nav-sidebar">
		            <li><a href="">Nav item again</a></li>
		            <li><a href="">One more nav</a></li>
		            <li><a href="">Another nav item</a></li>
	          	</ul>-->
	        </div>
	        <div class="col-sm-9 col-sm-offset-3 col-md-8 col-md-offset-3">
	        	@yield('content')
	        </div>
      	</div>
    </div>
	
	
</body>
</html>