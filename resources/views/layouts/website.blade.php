<!DOCTYPE html>
<html>
<head>
	<title>Revisteo</title>
	<meta name="viewport" content="width = 1050, user-scalable = no" />
	<link rel="stylesheet" type="text/css" href="{{ url('vendor/bootstrap/dist/css/bootstrap.min.css') }}" />
	<link rel="stylesheet" type="text/css" href="{{ url('/assets/stylesheets/website.css') }}" />
	<link rel="stylesheet" type="text/css" href="{{ url('/assets/stylesheets/flipbook.css') }}" />
	<script id="dsq-count-scr" src="//revisteo.disqus.com/count.js" async></script>

	<script type="text/javascript" src="{{ url('vendor/turnjs4/extras/modernizr.2.5.3.min.js') }}"></script>
	<script type="text/javascript" src="{{ url('vendor/turnjs4/extras/jquery.min.1.7.js') }}"></script>

	<script type="text/javascript" src="{{ url('vendor/turnjs4/lib/zoom.js') }}"></script>

	<script type="text/javascript" src="{{ url('vendor/jquery/dist/jquery.js') }}"></script>
	<script type="text/javascript" src="{{ url('vendor/bootstrap/dist/js/bootstrap.min.js') }}"></script>

	<!-- <script type="text/javascript" src="{{ url('vendor/turnjs4/lib/turn.js') }}"></script>
	<script type="text/javascript" src="{{ url('vendor/turnjs4/lib/turn.html4.min.js') }}"></script> -->

	<link rel="stylesheet" type="text/css" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,300' rel='stylesheet' type='text/css'>
	<script type="text/javascript" src="{{ url('vendor/turnjs4/samples/basic/js/basic.js') }}"></script>

</head>
<body>
	<nav class="nav-top navbar navbar-default navbar-fixed-top">
      	<div class="container">
			<div class="navbar-header">
	      		<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
		            <span class="sr-only">Toggle navigation</span>
		            <span class="icon-bar"></span>
		            <span class="icon-bar"></span>
		            <span class="icon-bar"></span>
	      		</button>
	      		<!--<a class="navbar-brand" href="#">Revisteo</a>-->
	    	</div>

	    	<div class="collapse navbar-collapse">
	          	<ul class="nav navbar-nav navbar-right">
	          		<li><a href="{{ route('panel.magazines.index') }}">Ir a panel de control</a></li>
	          		<li><a href="#">Iniciar sesión</a></li>
			        <li><a href="#">Registrarse</a></li>
	          	</ul>
	        </div><!-- /.navbar-collapse -->
       	</div>
    </nav>
	<nav id="nav-menu" class="nav-menu navbar navbar-default navbar-fixed-top">
      	<div class="container">
	        <div class="container-fluid">
	          	<div class="navbar-header">
		            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
		              	<span class="sr-only">Toggle navigation</span>
		              	<span class="icon-bar"></span>
		              	<span class="icon-bar"></span>
		              	<span class="icon-bar"></span>
		            </button>
		            <a class="navbar-brand" href="{{ route('website.index') }}">Revisteo</a>
	          	</div>
	          	<div id="navbar" class="navbar-collapse collapse">
		          	<form class="navbar-form navbar-right" style="margin-top:23px">
		            	<div class="input-group">
					      <input type="text" class="form-control" placeholder="">
					      <span class="input-group-btn">
					        <button class="btn btn-default" type="button">Buscar</button>
					      </span>
					    </div><!-- /input-group -->
		          	</form>
		            <ul class="nav navbar-nav navbar-right">
		              	<li class="active"><a href="./">Revistas mas leidas <span class="sr-only">(current)</span></a></li>
		              	<li><a href="../navbar-static-top/">Empresas</a></li>
		              	<li><a href="../navbar-fixed-top/">Categorias</a></li>
		            </ul>
	          	</div><!--/.nav-collapse -->
	        </div>
      	</div><!-- /.container-fluid -->
    </nav>
    <div class="container-page">
		@yield('content')
	</div>
	<footer class="footer">
      	<div class="container">
        	<p>Copyright ©  2015. All Rights Reserved.</p>
      	</div>
    </footer>
	<!-- <script type="text/javascript" src="{{ url() }}/vendor/bootbox.min.js"></script>
	<script type="text/javascript" src="{{ url() }}/vendor/jquery.form.js"></script>
	<script type="text/javascript" src="{{ url() }}/vendor/jquery.validate.min.js"></script>
	<script type="text/javascript" src="{{ url() }}/assets/javascripts/js.js"></script> -->
</body>
</html>