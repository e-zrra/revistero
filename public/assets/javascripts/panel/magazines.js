$(document).ready(function () {
	
	var element_magazine = $('#magazine_id');

	var first_element = new first_element_magazine();

	/* Sortable items */

	function first_element_magazine () {
		this.init = function () {
			$('#content-magazine-items div.col-md-3').find('.magazine-cover-page').remove();
			$('#content-magazine-items div.col-md-3:first-child').append('<span class="magazine-cover-page">portada</span>');
		}
	}

	first_element.init();
	
	$(function () {
		
		var prev_index = 0;

    	$("#content-magazine-items" ).sortable({
    		start: function (event, ui) {
    			prev_index = ui.item.index();
    		}, 
    		stop: function (event, ui) {

    			var data_id = $(ui.item).find('[data-id]').attr('data-id');
    			var _token = $('[name=_token]').val();
    			
    			var data = {
    				index: ui.item.index(),
    				prev_index: prev_index,
    				magazine_id: element_magazine.val(),
    				_token : _token
    			}

    			var notification = new Notification('#message-notification', 'Revista actualizada');
				var url = $('#url').val();
    			$.ajax({
					url: url + "/panel/magazines/item/update-index/" + data_id,
					data: data,
					dataType: "json",
					type: "PUT",
					beforeSend: function () {
					},
					error: function (response) {
						return null;
					},
					success: function (response, textStatus, jqXHR) {
						notification.show();
					},
					complete: function (response, textStatus, jqXHR) {
						notification.hide();
					}
				});
				first_element.init();
    		}
    	});
    	$("#sortable-magazine" ).disableSelection();
  	});

	jQuery.event.props.push("dataTransfer");

	/* Add Item to Magazine */
	$('#content-drag-drop-image').bind('drop', function (e) {
		var notification = new Notification('#message-notification', 'Almacenando ...');
			notification.show();

		var files = e.dataTransfer.files;
		var _token = $('[name=_token]').val();

		$.each(files, function (index, file) {
			var file_reader = new FileReader();
				file_reader.onload = (function(file) {
					return function(e) {
						var data = {
							name		: file.name,
							value 		: this.result,
							magazine_id : element_magazine.val(),
							_token : _token
						};
						upload_item(data);
					};
				})(files[index]);
			file_reader.readAsDataURL(file);
		});
		notification.update_text('Se almacenado correctamente');
		notification.hide();
	});

	function upload_item (data) {
		var url = $('#url').val();
		$.ajax({
			url: url + "/panel/magazines/item/store",
			data: data,
			dataType: "json",
			type: "POST",
			beforeSend: function () {
			},
			error: function (response) {
				return null;
			},
			success: function (response, textStatus, jqXHR) {
				if (response.error) {

					alert("Error: " + response.error);
				};
				if (response.data.id && response.data.image_url) {

					create_content_item(response.data.id, response.data.image_url);
				};
				first_element.init();
				return null;
			},
			complete: function (response, textStatus, jqXHR) {
			}
		});
	}

	function create_content_item (data_id, image_url) {

		var magazine 		= $('<div/>').prop('class', "content-magazine content-magazine-large mb40")
										 .css({'background-image':"url('" + image_url + "')", 'background-size' : 'cover'})
										 .attr('data-id', data_id);

		var content_delete 	= $('<a/>').prop('class', 'magazine-item-delete ion-android-delete color-danger pointer')
									   .text(' Eliminar ');

		var content_actions = $('<div/>').prop('class', 'magazine-content-actions')
										 .append(content_delete);

		var content_magazine = $('<div/>').prop('class', 'col-md-3')
										  .html(magazine.append(content_actions));

		$('#content-magazine-items').append(content_magazine);

		content_magazine.fadeOut().fadeIn('slow');
	}

	/* Delete item of the magazine */
	$('body').delegate('.magazine-item-delete', 'click', function () {

		var self 	= $(this), 
			data_id = self.closest('[data-id]').attr('data-id'),
			response = confirm("Are you sure to delete this?"),
			_token = $('[name=_token]').val();

		if (data_id == null) return false;
		var url = $('#url').val();
		if (response == true) {
			$.ajax({
				url: url + "/panel/magazines/item/destroy/" + data_id,
				dataType: "json",
				data:{ '_token' : _token },
				type: "DELETE",
				beforeSend: function () {
				},
				error: function (response) {
					return null;
				},
				success: function (response, textStatus, jqXHR) {
					if (response.error) {
						alert("Error: " + response.error);
					};
					if (response.success) {
						var notification = new Notification('#message-notification', response.success);
							notification.show();
						self.closest('.col-md-3').fadeOut(function () {
							$(this).remove();
						});
						notification.hide();
					};
					return null;
				},
				complete: function (response, textStatus, jqXHR) {
				}
			});
		};
	});

	var timeout;

	$('#form-magazine-edit').on('keypress paste cut', function (event)
	{
		var form = $(this);
		var notification = new Notification('#message-notification', '');

		if(timeout) {

			clearTimeout(timeout);
			timeout = null;
		}

		timeout = setTimeout(function () {
			$(form).ajaxSubmit({
	            beforeSend: function() {
	            },
	            error: function(r) {
	            },
	            success: function(response) {
	                if (response.msg) {
	                	notification.update_text(response.msg);
	                	notification.show();
	                } else {
	                	alert('Error');
	                	console.log(response);
	                }
	            },
	            complete: function() {
	            	notification.hide();
	            },
	            dataType: 'JSON',
	            type: 'PUT'
	        });
		}, 2000);
		
	});

});