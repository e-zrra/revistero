
/* Notification */

var Notification = function (element, text) {
	this.text 		= text;
	this.element 	= $(element);
}

$.extend(Notification.prototype, {
	show: function () {
		$(this.element).text(this.text).animate({ top: '10px'}, 500);
	},
	update_text: function (text) {
		this.text = text;
		$(this.element).text(this.text);
	},
	hide: function () {
		$(this.element).animate({ top: '-80px'}, 500);
	}
});

/* DragDrop */

function DragDrop () {

	this.init = function () {

		window.addEventListener("dragover",function(e){
			e = e || event;
			e.preventDefault();
		}, false);

		window.addEventListener("drop",function(e){
			e = e || event;
			e.preventDefault();
		}, false);
	}
};

/* Support */

/* FileReader */

if (window.FileReader) {

} else {

	alert('El navegador que esta usando no soporta algunas funcionalidades que integra la aplicación Web, recomendamos usar el navegador Google Chrome')
}