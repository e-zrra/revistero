{
  "name": "revisteo",
  "version": "0.0.1",
  "license": "MIT",
  "authors": [
    "none"
  ],
  "dependencies": {
  	"bootstrap": "~3.3.2",
    "modernizr": "~2.8.3",
    "jquery": "~1.10.2",
    "jquery-ui": "~1.10.4",
    "Chart.js": "~1.0.2"
  }
}
