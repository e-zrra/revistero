## Revistero App

### Vendor - Run
```
$ composer install
```

### Database
#### Create a database
```
$ mysql -u root -p
$ > create database revistero
$ > exit
```

### Rename .env file
	.env.example to .env

### Config .env file, replace the values for your localhost database config
 	DB_DATABASE, DB_USERNAME & DB_PASSWORD

### Run
```
$ php artisan key:generate
$ php artisan migrate
$ php artisan db:seed
```

### Run app
```
$ php artisan serve
```