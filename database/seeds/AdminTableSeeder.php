<?php

use Illuminate\Database\Seeder;

use App\User;

class AdminTableSeeder extends Seeder
{
    public function run()
    {
        User::create(array(
            'name' => 'Josh',
            'email' => 'host@host.com',
            'password' => 'password'
        ));
    }
}