<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(AdminTableSeeder::class);

        // factory('App\User', 50)->create();
        
        // $post = factory('App\Post', 5)->create();

        Model::reguard();
    }
}
